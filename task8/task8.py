import argparse
import numpy as np

PARSER = argparse.ArgumentParser()
PARSER.add_argument('path', help='Add file path of client_messages.json')
ARGS = PARSER.parse_args()


def func(path):
    data = []
    with open(path, 'r') as read_file:
        for val in read_file:
            data.append(int(val))
    return data


def main():
    data = func(ARGS.path)
    print(f"Mean: {np.mean(data):.2f}")
    print(f"Median: {np.median(data):.2f}")
    print(f"Min: {np.min(data):.2f}")
    print(f"Max: {np.max(data):.2f}")
    print(f"Standard Deviation: {np.std(data):.2f}")
    print(f"90th percentile: {np.percentile(data,90):.2f}")
    print(f"99th percentile: {np.percentile(data,99):.2f}")
    print(f"99.9th percentile: {np.percentile(data,99.9):.2f}")
    print(f"99.99th percentile: {np.percentile(data,99.99):.2f}")
    print(f"99.999th percentile: {np.percentile(data,99.999):.2f}")
    print(f"Total values: {np.size(data):.2f}\n")


main()
