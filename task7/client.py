import websockets
import asyncio
import json
import time
import logging
import argparse


PARSER = argparse.ArgumentParser()
PARSER.add_argument('path', help='give path of the client_message.json')
GROUP = PARSER.add_mutually_exclusive_group()
GROUP.add_argument('-c', action='store_true', help='Enable console logging')
GROUP.add_argument('-f', action='store_true', help='Enable file logging')

ARGS = PARSER.parse_args()

logger = logging.getLogger('client')
if ARGS.f == True:
    logging.basicConfig(
        filename='msg_communication.log', level=logging.INFO, filemode='w')
else:
    logging.basicConfig(level=logging.INFO)


async def listen(path):
    url = "ws://127.0.0.1:7890"
    with open(path, 'r') as read_file:
        json_data = json.load(read_file)
    async with websockets.connect(url) as ws:
        for sending_message in json_data:
            sending_message['tx_time'] = time.time_ns()
            logger.info("Message is sending to server:")
            await ws.send(json.dumps(sending_message))
            recieving_message = await ws.recv()
            logger.info("Message is recieved from server:" + recieving_message)
            recieving_message = json.loads(recieving_message)
            with open('msg_latency.txt', 'a') as write_file:
                write_file.write(str(recieving_message['msg_latency']) + "\n")
                logger.info(
                    f"Message latency is {recieving_message['msg_latency']}.")
                logger.info("-----------------------------------------------")
                write_file.close()


asyncio.get_event_loop().run_until_complete(listen(ARGS.path))
