import websockets
import asyncio
import json
import time
import logging
import argparse

PORT = 7890

PARSER = argparse.ArgumentParser()
GROUP = PARSER.add_mutually_exclusive_group()
GROUP.add_argument('-c', action='store_true', help='Enable console logging')
GROUP.add_argument('-f', action='store_true', help='Enable file logging')
ARGS = PARSER.parse_args()

logger = logging.getLogger('server')
if ARGS.f == True:
    logging.basicConfig(
        filename='msg_communication.log', level=logging.INFO, filemode='w')
else:
    logging.basicConfig(level=logging.INFO)


async def echo(websocket, path):
    logger.info("A client is connected")
    async for json_message in websocket:
        logger.info("Recieved message from client: " + json_message)
        message = json.loads(json_message)
        message['rx_time'] = time.time_ns()
        message['msg_latency'] = message['rx_time'] - message['tx_time']
        message = json.dumps(message)
        logger.info("\nSending message to client: " + message)
        await websocket.send(message)


start_server = websockets.serve(echo, "localhost", PORT)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
