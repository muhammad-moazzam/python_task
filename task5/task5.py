from typing import List
from matplotlib import colors
import matplotlib.pyplot as plt


def square(list):
    square_list = [None] * len(list)
    for index, val in enumerate(list):
        square_list[index] = val**2
    return square_list


def cube(list):
    cube_list = [None] * len(list)
    for index, val in enumerate(list):
        cube_list[index] = val**3
    return cube_list


def main():
    list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    plt.plot(list, square(list), color="r", label='square')
    plt.plot(list, cube(list), color='g', label='cube')
    plt.title("Square and cubic functions")
    plt.legend()
    plt.show()


main()
