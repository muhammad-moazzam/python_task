import argparse
import json
import random
import math

PARSER = argparse.ArgumentParser()
GROUP = PARSER.add_mutually_exclusive_group()
GROUP.add_argument('-i', help="Number of iterations", action="store_true")
GROUP.add_argument(
    '-j', help="Reads number of iterations from JSON file", action="store_true")
ARGS = PARSER.parse_args()


def monte_carlo_simulation(iteration: int):
    circle_score = 0
    square_score = 0
    for i in range(iteration):
        x_coordinate = random.uniform(-1.0, 1.0)
        y_coordinate = random.uniform(-1.0, 1.0)
        dis = math.sqrt((x_coordinate**2) + (y_coordinate**2))

        if dis <= 1:
            circle_score += 1
        square_score += 1

    pi = 4 * (circle_score / square_score)
    print(f"The value of pi: {pi}")


def main():
    if ARGS.i == True:
        iteration = int(input("Please Enter Number of Iterations: "))
        monte_carlo_simulation(iteration)
    elif ARGS.j == True:
        with open("data_file.json", "r") as write_file:
            iteration = json.load(write_file)
            monte_carlo_simulation(iteration)
    else:
        print("use -h for flag guidance")


main()
