import os
import psutil


def check_directory():
    os.chdir("/home/emumba/")
    arr = os.listdir()
    if "Details" in arr:
        os.chdir("/home/emumba/Details")
    else:
        os.mkdir("Details/")
        os.chdir("/home/emumba/Details")


def main():
    check_directory()
    ram_memory = int((psutil.virtual_memory().total)/(1024*1024))
    system_info = os.popen("lscpu")
    with open("Summary.txt", "w") as write_file:
        write_file.write(system_info.read())
        write_file.write(f"RAM Memory: {ram_memory}")


main()
