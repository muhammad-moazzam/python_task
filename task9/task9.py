
def get_common_words(num_of_words, count):
    for i in range(num_of_words):
        word = input(f"Enter word {i+1}: ")
        if word in count:
            count[word] += 1
        else:
            count[word] = 1


def print_common_words(count):
    for i in count:
        print(count[i])


def main():
    num_of_words = int(input("Enter number of words: "))
    count = {}
    get_common_words(num_of_words, count)
    print_common_words(count)


main()
