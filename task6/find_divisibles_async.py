import asyncio
import time
import logging
from pprint import pprint

logging.basicConfig(level=logging.INFO)


async def find_divisibles_async(number_range: int, divisor: int):
    list = []
    start_time = time.time()
    logging.info(
        f"find_divisibles called with range {number_range} and divisor {divisor}")
    for val in range(1, number_range+1):
        if (val % divisor == 0):
            list.append(val)
            await asyncio.sleep(0.001)
    end_time = time.time()
    logging.info(
        f"find_divisibles ended with range {number_range} and divisor {divisor}. It took {end_time - start_time} seconds")
    return list


async def main():
    task1 = asyncio.create_task(find_divisibles_async(50800000, 34113))
    task2 = asyncio.create_task(find_divisibles_async(100052, 3210))
    task3 = asyncio.create_task(find_divisibles_async(500, 3))
    list1 = await task1
    list2 = await task2
    list3 = await task3
    print("\n(50800000, 34113): ")
    # pprint(list1)
    print("\n(100052, 3210): ")
    pprint(list2)
    print("\n(500, 3): ")
    pprint(list3)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
