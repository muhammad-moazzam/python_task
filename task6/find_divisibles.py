import logging
import time
from pprint import pprint


logging.basicConfig(level=logging.INFO)


def find_divisibles(in_range: int, divisor: int):
    list = []
    start_time = time.time()
    logging.info(
        f"find_divisibles called with range {in_range} and divisor {divisor}")
    for val in range(1, in_range+1):
        if (val % divisor == 0):
            list.append(val)
    end_time = time.time()
    logging.info(
        f"find_divisibles ended with range {in_range} and divisor {divisor}. It took {end_time - start_time} seconds")
    return list


def main():

    list1 = find_divisibles(50800000, 34113)
    list2 = find_divisibles(100052, 3210)
    list3 = find_divisibles(500, 3)
    print("\n(50800000, 34113): ")
    pprint(list1)
    print("\n(100052, 3210): ")
    pprint(list2)
    print("\n(500, 3): ")
    pprint(list3)


main()
