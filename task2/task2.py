# import argparse
import argparse
import math

PARSER = argparse.ArgumentParser()
PARSER.add_argument(
    "UP", help="distance covered in the upward direction", type=int)
PARSER.add_argument(
    "DOWN", help="distance covered in the downward direction", type=int)
PARSER.add_argument(
    "RIGHT", help="distance covered in the RIGHT direction", type=int)
PARSER.add_argument(
    "LEFT", help="distance covered in the LEFT direction", type=int)
DIRECTION = PARSER.parse_args()


def vertical_movement_calculator() -> float:
    return DIRECTION.UP - DIRECTION.DOWN


def horizontal_movement_calculator() -> float:
    return DIRECTION.RIGHT - DIRECTION.LEFT


def distance_calculator():
    vertical_movement = vertical_movement_calculator()
    horizontal_movement = horizontal_movement_calculator()

    distance = math.sqrt((vertical_movement)**2 + (horizontal_movement)**2)
    print(round(distance))


distance_calculator()
